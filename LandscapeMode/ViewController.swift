//
//  ViewController.swift
//  LandscapeMode
//
//  Created by Syncrhonous on 23/3/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var text: String = "1234 kbps"
    let defaults = UserDefaults.standard
    var value: String = ""
     var timeCounter: UInt32 = 0
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textF: UITextField!
    var timer = Timer()
    
    
    @IBOutlet weak var slider: UISlider!{
        didSet{
            slider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        }
    }
    
    @IBOutlet weak var slider2: UISlider!{
        didSet{
            slider2.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        }
    }
    
    
    	
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload")
        textLabel.text = "he he ehe"
        self.hideKeyboardWhenTappedAround()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
       

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear")
    }

    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("view will disappear")
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("view did disappear")
    }
    
    
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
    }


    @IBAction func btnClicked(_ sender: UIButton) {
        //substring
        //let subS = String(text.dropLast(5))
        //print("subS:>\(subS)>")
//       var stringOne = defaults.string(forKey: "dataone")
//
//        if stringOne == "" {
//            print("stringOne: ans null")
//        }else{
//            print("stringOne: \(stringOne!)")
//        }
        
        let textValue: String = textF.text!
        
        if textValue == "" {
            print("Text field should not empty")
        }else{
            defaults.set(textF.text, forKey: "dataone")
            print("data stored")
        }
    }
    
    
    @IBAction func displayData(_ sender: UIButton) {
        value = defaults.string(forKey: "dataone") ?? ""
        if value == "" {
            print("value = null")
        }else{
            print("value: \(value)")
            print("value: \(value + " bang")")
        }
    }
    
    open func backM(){
        print("background")
        //textF.background =
        textLabel?.text = "back"
    }
    
    
    open func foreM(){
        print("foreground")
        textLabel?.text = "fore"
    }
    

    
    func timeCounterMethod(){
        print("First: observeValueffff")
        timeCounter = timeCounter + 1
        let seconds = timeCounter % 10
        var minutes = timeCounter / 10
        let hours = minutes / 10
        minutes = minutes % 10
        textLabel?.text = String("time: \(hours) : \(minutes) : \(seconds)")
    }
    
    
    
    
    
    
    
    
    
    
    
}


//extension for hiding keyboard on outside touch
extension ViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}





